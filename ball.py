import pygame
import random


class Ball:

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.width = 10
        self.vx = vx
        self.vy = vy

    def draw(self, win):
        pygame.draw.circle(win, (255, 255, 255), (self.x, self.y), self.width)

    def reset(self, game):
        self.x = game.width // 2
        self.y = game.height // 2
        pygame.event.clear()
        game.clicked = False
        # we don't want vx = 0 and we don't want it to be too slow
        self.vx = random.choice([-5, -4, 4, 5])

        # we want the ball to start by going in the direction of the player
        self.vy = random.randrange(4, 6)

    def move(self):
        self.x += self.vx
        self.y += self.vy

    def collide_with_bounds(self, game):
        if self.x - self.width // 2 < 0 or self.x + self.width // 2 > game.width:
            self.vx = -self.vx
        if self.y - self.width // 2 < 0:   # the other bound is loss
            self.vy = -self.vy

    def collide(self, obj):
        # can check collision with any object that has x, y, width and height

        touched = False
        # if the ball hits the object by the side
        if ((obj.y - obj.height // 2 <= self.y <= obj.y + obj.height // 2) and
            ((obj.x - obj.width // 2 < self.x - self.width // 2 <= obj.x + obj.width // 2)
                or (obj.x - obj.width // 2 < self.x + self.width // 2 < obj.x + obj.width//2))):
            self.vx = -self.vx
            touched = True

        # if the ball hits the object by the top
        if (obj.x - obj.width // 2 <= self.x <= obj.x + obj.width // 2) \
            and ((self.y + self.width // 2 >= obj.y - obj.height // 2 and self.y - self.width // 2 < obj.y + obj.height // 2)
             or (self.y - self.width // 2 <= obj.y + obj.height // 2 and self.y - self.width // 2 > obj.y - obj.height // 2)):
            self.vy = -self.vy
            touched = True

        return touched

    # # unused atm
    # def increase_speed(self, increase):
    #     self.vx = self.vx + increase if self.vx >= 0 else self.vx - increase
    #     self.vy = self.vy + increase if self.vy >= 0 else self.vy - increase

