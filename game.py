import pygame
from player import Player
from ball import Ball
from block import Block


class Game:
    def __init__(self):
        pygame.init()
        self.width = 1600
        self.height = 900
        self.win = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption("BrickBreak")
        self.player = Player(self.width // 2, self.height - 50)
        self.balls = []     # contains all the balls of the game
        self.blocks = []
        self.clicked = False    # allow to wait for click to start the game

    def draw(self):
        self.win.fill((0, 0, 0))

        self.player.draw(self.win)

        for ball in self.balls:
            ball.draw(self.win)

        for block in self.blocks:
            block.draw(self.win)

        pygame.display.update()

    def initialize(self):
        first_ball = Ball(0, 0, 0, 0)
        first_ball.reset(self)
        self.balls.append(first_ball)
        self.blocks.extend([Block(400, 400), Block(500, 500)])

    def play(self):
        self.initialize()

        clock = pygame.time.Clock()

        run = True
        # game loop
        while run:
            clock.tick(60)
            (mouse_x, mouse_y) = pygame.mouse.get_pos()
            self.player.x = mouse_x

            if self.clicked:
                for ball in self.balls:
                    ball.move()
                    ball.collide_with_bounds(self)
                    ball.collide(self.player)
                    for block in self.blocks:
                        if ball.collide(block):
                            block.life -= 1
                            if block.life <= 0:
                                self.blocks.remove(block)
                                del block

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    self.clicked = True

            self.draw()

        pygame.quit()


# main program
game = Game()
game.play()
