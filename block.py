import pygame


class Block:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.width = 80
        self.height = 80
        self.life = 1

    def draw(self, win):
        color = (204, 102, 0)   # yellow-brown
        pygame.draw.rect(win, color, (self.x - self.width // 2, self.y - self.height // 2, self.width, self.height))
