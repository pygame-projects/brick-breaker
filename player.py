import pygame


class Player:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.width = 200
        self.height = 30

    def draw(self, win):
        pygame.draw.rect(win,
                         (255, 255, 255),
                         (self.x - self.width //2, self.y - self.height//2, self.width, self.height))

